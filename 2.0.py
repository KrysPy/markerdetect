"""
Detecting marker on laparoscopy tool

Potential:
blue_lower = np.array([100,150,0], dtype='uint8')
blue_upper = np.array([140,255,255], dtype='uint8')

blue_lower = np.array([94,80,2], dtype='uint8')
blue_upper = np.array([126, 255, 255], dtype='uint8')


BUGS:
- Multi-windows,
- Full-screen
"""

import argparse
import numpy as np
import cv2
import time

# defining lower and upper boundaries of markers color
lower_red = np.array([161, 155, 84])
upper_red = np.array([179, 255, 255])

window_name = 'Camera'
kernel = np.ones((3, 3), np.uint8)


# parser to change camera src from terminal
parser = argparse.ArgumentParser(description="Choose camera index.")
parser.add_argument('-c', '--camera',
                    type=int, metavar='', help='Camera index')
args = vars(parser.parse_args())

# capturing video
if not args.get('camera', False):
    captured_video = cv2.VideoCapture(0)
else:
    captured_video = cv2.VideoCapture(args['camera'])


# time to run camera
time.sleep(4)

while True:
    # capturing current frame
    _, frame = captured_video.read()

    # get resolution of displaying window
    windowWidth = frame.shape[1]
    windowHeight = frame.shape[0]

    # video processing
    blurred_video = cv2.GaussianBlur(frame, (21, 21), 0)
    hsv_format = cv2.cvtColor(blurred_video, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv_format, lower_red, upper_red)
    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel=kernel)
    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel=kernel)
    mask = cv2.dilate(mask, kernel=kernel, iterations=1)

    # color on black background
    # result = cv2.bitwise_and(frame, frame, mask=mask)

    _, binary = cv2.threshold(mask, 127, 255, cv2.THRESH_BINARY)

    # finding contours
    cnt, _ = cv2.findContours(binary, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    if len(cnt) > 0:
        for c in cnt:
            area = cv2.contourArea(c)
            if area > 300:
                # straight rectangle
                x, y, w, h = cv2.boundingRect(c)
                frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), thickness=1)

                # draw actual contour of item
                # cv2.drawContours(frame, [c], -1, (255, 0, 0), 2)

                print(f"Area of contour (not bounding box): {area}")

    # video infos
    endx_info_box = round(windowWidth*0.3)
    endy_info_box = round(windowHeight * 0.45)
    cv2.rectangle(frame, (0, 0), (endx_info_box, endy_info_box), (255, 0, 0), 2)

    cv2.imshow(window_name, frame)

    key = cv2.waitKey(1)
    # if the 'q' key is pressed, stop the loop
    if key == ord("q"):
        break

captured_video.release()
cv2.destroyAllWindows()
