# MarkerDetect

Program pozwala na znalezienie na obrazie z kamery markera o określonym kolorze, który znajduje się na używanym narzędziu laparoskopowym. Następnie opisuje na odnalezionym markerze prostokąt oraz koło. Następnie wyświetla współrzędne środka opisanego koła oraz jego promień. 